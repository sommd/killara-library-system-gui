package com.djs.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Utils for reading and writing files.
 *
 * @author David Sommerich - 29/06/15
 */
public class FileUtils{
	/**
	 * Reads a file and returns the lines of it.
	 *
	 * @param file The file to read.
	 * @param errorMessage The message to print if an error occurs.
	 *
	 * @return The lines in the file if it could be opened, or <code>null</code> if an error occurred.
	 */
	public static List<String> readLines(File file, String notFoundMessage, String errorMessage){
		List<String> lines = null;
		
		try(BufferedReader reader = new BufferedReader(new FileReader(file))){
			lines = new ArrayList<>();
			
			String line;
			while((line = reader.readLine()) != null){
				lines.add(line);
			}
		}catch(FileNotFoundException e){
			System.err.println(notFoundMessage);
			return null;
		}catch(IOException e){
			System.err.println(errorMessage);
			return null;
		}
		
		return lines;
	}
	
	/**
	 * Opens a file as a PrintStream.
	 *
	 * @param file The file to open.
	 * @param errorMessage The message to print if the file could not be opened.
	 *
	 * @return A {@link PrintStream} to write to the file.
	 */
	public static PrintStream writeFile(File file, String errorMessage){
		PrintStream s = null;
		
		try{
			s = new PrintStream(file);
		}catch(FileNotFoundException e){
			System.err.println(errorMessage);
		}
		
		return s;
	}
}
