package com.djs.assignment2;

/**
 * Represents a book in the library.
 *
 * @author David Sommerich - 28/06/15
 */
public class Book{
	private int year, number;
	private String author, title, publisher;
	
	/**
	 * Create a book with no fields set.
	 */
	public Book(){
		author = title = publisher = "";
	}
	
	/**
	 * Create a book with the specified fields.
	 *
	 * @param year The year.
	 * @param author The author.
	 * @param title The title.
	 * @param publisher The publisher.
	 */
	public Book(int number, int year, String author, String title, String publisher){
		setNumber(number);
		setYear(year);
		setAuthor(author);
		setTitle(title);
		setPublisher(publisher);
	}
	
	/**
	 * @return The book number.
	 */
	public int getNumber(){
		return number;
	}
	
	/**
	 * @param number The new book number.
	 */
	public void setNumber(int number){
		this.number = number;
	}
	
	/**
	 * @return The year.
	 */
	public int getYear(){
		return year;
	}
	
	/**
	 * @param year The new year.
	 */
	public void setYear(int year){
		this.year = year;
	}
	
	/**
	 * @return The author.
	 */
	public String getAuthor(){
		return author;
	}
	
	/**
	 * @param author The new author.
	 */
	public void setAuthor(String author){
		this.author = author;
	}
	
	/**
	 * @return The title.
	 */
	public String getTitle(){
		return title;
	}
	
	/**
	 * @param title The new title.
	 */
	public void setTitle(String title){
		this.title = title;
	}
	
	/**
	 * @return The publisher.
	 */
	public String getPublisher(){
		return publisher;
	}
	
	/**
	 * @param publisher The new publisher.
	 */
	public void setPublisher(String publisher){
		this.publisher = publisher;
	}
}
