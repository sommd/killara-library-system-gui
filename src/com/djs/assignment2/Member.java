package com.djs.assignment2;

/**
 * Represents a member in the library.
 *
 * @author David Sommerich - 29/06/15
 */
public class Member{
	private String name, address, phoneNumber;
	private int postcode;
	
	/**
	 * Create a member with no fields set.
	 */
	public Member(){
		name = address = phoneNumber = "";
	}
	
	/**
	 * Create a member with the specified fields.
	 *
	 * @param name The member's name.
	 * @param address The member's address.
	 * @param postcode The member's postcode.
	 * @param phoneNumber The member's phone number.
	 */
	public Member(String name, String address, int postcode, String phoneNumber){
		setName(name);
		setAddress(address);
		setPostcode(postcode);
		setPhoneNumber(phoneNumber);
	}
	
	/**
	 * @return The member's name.
	 */
	public String getName(){
		return name;
	}
	
	/**
	 * @param name The member's new name.
	 */
	public void setName(String name){
		this.name = name;
	}
	
	/**
	 * @return The member's address.
	 */
	public String getAddress(){
		return address;
	}
	
	/**
	 * @param address The member's new address.
	 */
	public void setAddress(String address){
		this.address = address;
	}
	
	/**
	 * @return The member's phoneNumber.
	 */
	public String getPhoneNumber(){
		return phoneNumber;
	}
	
	/**
	 * @param phoneNumber The member's new phoneNumber.
	 */
	public void setPhoneNumber(String phoneNumber){
		this.phoneNumber = phoneNumber;
	}
	
	/**
	 * @return The member's postcode.
	 */
	public int getPostcode(){
		return postcode;
	}
	
	/**
	 * @param postcode The member's new postcode.
	 */
	public void setPostcode(int postcode){
		this.postcode = postcode;
	}
}
