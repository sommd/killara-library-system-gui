package com.djs.assignment2;

import com.djs.utils.FileUtils;

import java.io.File;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

public class Library{
	private static final File BOOKS_FILE = new File("books.txt");
	private static final File MEMBERS_FILE = new File("members.txt");
	
	private List<Book> books;
	private List<Member> members;
	
	public Library(){
		this.books = new ArrayList<>();
		this.members = new ArrayList<>();
		
		loadBooks();
		loadMembers();
	}
	
	private void loadBooks(){
		List<String> lines = FileUtils.readLines(BOOKS_FILE, "No books file...", "Error reading books file...");
		
		if(lines != null){
			try{
				Iterator<String> i = lines.iterator();
				
				while(i.hasNext()){
					String number = i.next();
					String author = i.next();
					String title = i.next();
					String publisher = i.next();
					String year = i.next();
					
					books.add(new Book(Integer.parseInt(number), Integer.parseInt(year), author, title, publisher));
				}
			}catch(NoSuchElementException ignore){
			}catch(NumberFormatException e){
				System.err.println("Error reading books file, not all books may have been loaded...");
			}
		}
	}
	
	private void loadMembers(){
		List<String> lines = FileUtils.readLines(MEMBERS_FILE, "No members file...", "Error reading members file...");
		
		if(lines != null){
			try{
				Iterator<String> i = lines.iterator();
				
				while(i.hasNext()){
					String name = i.next();
					String address = i.next();
					String postcode = i.next();
					String phoneNumber = i.next();
					
					members.add(new Member(name, address, Integer.parseInt(postcode), phoneNumber));
				}
			}catch(NoSuchElementException ignore){
			}catch(NumberFormatException e){
				System.err.println("Error reading members file, not all members may have been loaded...");
			}
		}
	}
	
	/**
	 * @return <code>true</code> if both the books and members were successfully saved, <code>false</code> otherwise.
	 */
	public boolean save(){
		return writeBooks() && writeMembers();
	}
	
	/**
	 * @return <code>true</code> if the books were successfully saved, <code>false</code> otherwise.
	 */
	private boolean writeBooks(){
		PrintStream stream = FileUtils.writeFile(BOOKS_FILE, "Books could not be saved...");
		
		if(stream == null){
			return false;
		}
		
		for(Book book: books){
			stream.println(book.getNumber());
			stream.println(book.getAuthor());
			stream.println(book.getTitle());
			stream.println(book.getPublisher());
			stream.println(book.getYear());
		}
		
		stream.close();
		
		return !stream.checkError();
	}

	/**
	 * @return <code>true</code> if the members were successfully saved, <code>false</code> otherwise.
	 */
	private boolean writeMembers(){
		PrintStream stream = FileUtils.writeFile(MEMBERS_FILE, "Members could not be saved...");
		
		if(stream == null){
			return false;
		}
		
		for(Member member: members){
			stream.println(member.getName());
			stream.println(member.getAddress());
			stream.println(member.getPostcode());
			stream.println(member.getPhoneNumber());
		}
		
		stream.close();
		
		return !stream.checkError();
	}
	
	public List<Book> getBooks(){
		return books;
	}
	
	public List<Member> getMembers(){
		return members;
	}
}
