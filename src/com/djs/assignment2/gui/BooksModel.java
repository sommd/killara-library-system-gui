package com.djs.assignment2.gui;

import com.djs.assignment2.Book;

import java.util.List;

import javax.swing.table.AbstractTableModel;

public class BooksModel extends AbstractTableModel{
	private static final int NUM_FIELDS = 5;
	
	private static final int FIELD_NUMBER = 0;
	private static final int FIELD_TITLE = 1;
	private static final int FIELD_AUTHOR = 2;
	private static final int FIELD_PUBLISHER = 3;
	private static final int FIELD_YEAR = 4;
	
	private final List<Book> books;
	
	public BooksModel(List<Book> books){
		this.books = books;
	}
	
	@Override
	public int getRowCount(){
		return books.size();
	}
	
	@Override
	public int getColumnCount(){
		return NUM_FIELDS;
	}
	
	@Override
	public Object getValueAt(int rowIndex, int columnIndex){
		Book book = books.get(rowIndex);
		
		switch(columnIndex){
			case FIELD_NUMBER:
				return book.getNumber();
			case FIELD_TITLE:
				return book.getTitle();
			case FIELD_AUTHOR:
				return book.getAuthor();
			case FIELD_PUBLISHER:
				return book.getPublisher();
			case FIELD_YEAR:
				return book.getYear();
			default:
				return null;
		}
	}
	
	@Override
	public String getColumnName(int column){
		switch(column){
			case FIELD_NUMBER:
				return "Number";
			case FIELD_TITLE:
				return "Title";
			case FIELD_AUTHOR:
				return "Author";
			case FIELD_PUBLISHER:
				return "Publisher";
			case FIELD_YEAR:
				return "Year";
			default:
				return null;
		}
	}
	
	@Override
	public void setValueAt(Object value, int rowIndex, int columnIndex){
		Book book = books.get(rowIndex);
		
		switch(columnIndex){
			case FIELD_NUMBER:
				book.setNumber((int) value);
				break;
			case FIELD_TITLE:
				book.setTitle((String) value);
				break;
			case FIELD_AUTHOR:
				book.setAuthor((String) value);
				break;
			case FIELD_PUBLISHER:
				book.setPublisher((String) value);
				break;
			case FIELD_YEAR:
				book.setYear((int) value);
				break;
		}
	}
	
	@Override
	public Class<?> getColumnClass(int columnIndex){
		switch(columnIndex){
			case FIELD_NUMBER:
			case FIELD_YEAR:
				return Integer.class;
			case FIELD_TITLE:
			case FIELD_AUTHOR:
			case FIELD_PUBLISHER:
				return String.class;
			default:
				return null;
		}
	}
	
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex){
		return true;
	}
}
