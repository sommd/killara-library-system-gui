package com.djs.assignment2.gui;

import com.djs.assignment2.Member;

import java.util.List;

import javax.swing.table.AbstractTableModel;

public class MembersModel extends AbstractTableModel{
	private static final int NUM_FIELDS = 4;
	
	private static final int FIELD_NAME = 0;
	private static final int FIELD_ADDRESS = 1;
	private static final int FIELD_POST_CODE = 2;
	private static final int FIELD_PHONE_NUMBER = 3;
	
	private final List<Member> members;
	
	public MembersModel(List<Member> members){
		this.members = members;
	}
	
	@Override
	public int getRowCount(){
		return members.size();
	}
	
	@Override
	public int getColumnCount(){
		return NUM_FIELDS;
	}
	
	@Override
	public Object getValueAt(int rowIndex, int columnIndex){
		Member member = members.get(rowIndex);
		
		switch(columnIndex){
			case FIELD_NAME:
				return member.getName();
			case FIELD_ADDRESS:
				return member.getAddress();
			case FIELD_POST_CODE:
				return member.getPostcode();
			case FIELD_PHONE_NUMBER:
				return member.getPhoneNumber();
			default:
				return null;
		}
	}
	
	@Override
	public String getColumnName(int column){
		switch(column){
			case FIELD_NAME:
				return "Name";
			case FIELD_ADDRESS:
				return "Address";
			case FIELD_POST_CODE:
				return "Post Code";
			case FIELD_PHONE_NUMBER:
				return "Phone Number";
			default:
				return null;
		}
	}
	
	@Override
	public void setValueAt(Object value, int rowIndex, int columnIndex){
		Member member = members.get(rowIndex);
		
		switch(columnIndex){
			case FIELD_NAME:
				member.setName((String) value);
				break;
			case FIELD_ADDRESS:
				member.setAddress((String) value);
				break;
			case FIELD_POST_CODE:
				member.setPostcode((int) value);
				break;
			case FIELD_PHONE_NUMBER:
				member.setPhoneNumber((String) value);
				break;
		}
	}
	
	@Override
	public Class<?> getColumnClass(int columnIndex){
		switch(columnIndex){
			case FIELD_POST_CODE:
				return Integer.class;
			case FIELD_NAME:
			case FIELD_ADDRESS:
			case FIELD_PHONE_NUMBER:
				return String.class;
			default:
				return null;
		}
	}
	
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex){
		return true;
	}
}
