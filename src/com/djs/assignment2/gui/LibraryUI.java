package com.djs.assignment2.gui;

import com.djs.assignment2.Book;
import com.djs.assignment2.Library;
import com.djs.assignment2.Member;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Arrays;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableRowSorter;

public class LibraryUI implements ActionListener{
	private static final String WINDOW_TITLE = "Killara Library Management System";
	
	private static final int WINDOW_WIDTH = 640;
	private static final int WINDOW_HEIGHT = 480;
	
	public static void main(String[] args){
		try{
			// make the UI theme look 'native', e.g. the windows theme or mac theme instead of the java theme
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		}catch(Exception ignore){}
		
		// load the library
		final Library library = new Library();
		
		// create a window
		JFrame frame = new JFrame(WINDOW_TITLE);
		// set the window to destroy itself when it closes, so that the program finishes
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		// set the window to show this form
		frame.setContentPane(new LibraryUI(library).mainPanel);
		
		frame.setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
		// center the window on the screen
		frame.setLocationRelativeTo(null);
		// show the window on the screen
		frame.setVisible(true);
		
		// add a listener to listen for the window to be closed
		frame.addWindowListener(new WindowAdapter(){
			@Override
			public void windowClosing(WindowEvent e){
				// save the library when the window is closed
				library.save();
			}
		});
	}
	
	private JTable booksTable;
	private JTable membersTable;
	private JPanel mainPanel;
	private JButton deleteBookButton;
	private JButton newBookButton;
	private JButton newMemberButton;
	private JButton deleteMemberButton;
	
	private Library library;
	private BooksModel booksModel;
	private MembersModel membersModel;
	
	public LibraryUI(Library library){
		this.library = library;
		
		// create the books model which provides the table with the cell data from the books in the library
		booksModel = new BooksModel(library.getBooks());
		// set the table to use the model
		booksTable.setModel(booksModel);
		// add a sorter so that clicking the column titles sorts the rows
		booksTable.setRowSorter(new TableRowSorter<>(booksModel));
		
		// same thing for the members table
		membersModel = new MembersModel(library.getMembers());
		membersTable.setModel(membersModel);
		membersTable.setRowSorter(new TableRowSorter<>(membersModel));
		
		// add a listener to all the buttons so it calls this class to handle them when they're clicked
		newBookButton.addActionListener(this);
		deleteBookButton.addActionListener(this);
		newMemberButton.addActionListener(this);
		deleteMemberButton.addActionListener(this);
	}
	
	/**
	 * Adds a new row to the table and selects it
	 */
	public <T> void addNewRow(JTable table, T newItem, List<T> list, AbstractTableModel model){
		// add the new item>
		list.add(newItem);
		
		int row = list.size() - 1;
		// notify the table that the new row was added
		model.fireTableRowsInserted(row, row);
		// select the new row
		table.changeSelection(row, 0, false, false);
		// give the table focus so that the user can start typing and it edits the values in the table
		table.requestFocus();
	}
	
	/**
	 * Deletes the rows selected in the table and removes the from the list after confirming with hte user
	 */
	private void deleteSelectedRows(JTable table, List<?> list, AbstractTableModel model){
		// get the selected rows
		int[] rows = table.getSelectedRows();
		
		// if there are rows selected
		if(rows.length > 0){
			// show a dialog asking if the user is sure they want to delete the selected rows
			// the user's response (yes or no) is stored in 'result'
			int result = JOptionPane.showConfirmDialog(
					// the parent of the dialog, so that the dialog is always on top
					mainPanel,
					"Are you sure you want to delete the selected rows?", // message
					"Delete rows", // title
					// specify that the only options are 'yes' and 'no'
					JOptionPane.YES_NO_OPTION);
			
			// the user pressed the yes button
			if(result == JOptionPane.YES_OPTION){
				// sort the rows so they are in increasing order
				Arrays.sort(rows);
				
				// iterate array in a decreasing order because removing items before other items changes their indices which
				
				
				// results in removing the wrong items
				for(int i = rows.length - 1; i >= 0; i--){
					int row = rows[i];
					
					// remove the row
					list.remove(row);
					// notify the table that the row was removed
					model.fireTableRowsDeleted(row, row);
				}
			}
		}else{ // there aren't any selected rows
			JOptionPane.showMessageDialog(
					// the parent of the dialog, so that the dialog is always on top
					mainPanel,
					"Select the row(s) you want to delete by clicking them", // message
					"No rows selected", // title
					// show the dialog as an error message
					JOptionPane.ERROR_MESSAGE);
		}
	}
	
	/**
	 * Called when one of the buttons this class is listeneing to is pressed
	 */
	@Override
	public void actionPerformed(ActionEvent e){
		if(e.getSource() == newBookButton){ // new book button was clicked
			// add a new book
			addNewRow(booksTable, new Book(), library.getBooks(), booksModel);
		}else if(e.getSource() == deleteBookButton){ // delete book button was pressed
			// delete the selected books
			deleteSelectedRows(booksTable, library.getBooks(), booksModel);
		}else if(e.getSource() == newMemberButton){ // new member button was press
			// add a new member
			addNewRow(membersTable, new Member(), library.getMembers(), membersModel);
		}else if(e.getSource() == deleteMemberButton){ // delete member button was pressed
			// delete the selected members
			deleteSelectedRows(membersTable, library.getMembers(), membersModel);
		}
	}
}
